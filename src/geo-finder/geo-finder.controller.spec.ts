import { HttpModule } from '@nestjs/axios';
import { CacheModule, INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AddressToCoordinateService } from '../address-to-coordinate/address-to-coordinate.service';
import { DistrictService } from '../district/district.service';
import { GeoFinderController } from './geo-finder.controller';
import { when } from "jest-when";
import request from 'supertest';

describe('GeoFinderController', () => {
  let server: INestApplication;

  const response = {
      send: (body?: any) => {}, 
      status: (code: number) => response,
  }

  let mockAddressToCoordinateService = { getCoordinatesForAddress: jest.fn() };
  let mockDistrictService = { findDistrict: jest.fn() };

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [GeoFinderController],
      providers: [AddressToCoordinateService, DistrictService],
      imports: [ CacheModule.register({ ttl: 60, max: 1000 }), ConfigModule, HttpModule]
    })
    .overrideProvider(AddressToCoordinateService)
    .useValue(mockAddressToCoordinateService)
    .overrideProvider(DistrictService)
    .useValue(mockDistrictService)
    .compile();

    server = app.createNestApplication();
    server.init();
  });

  afterAll(() => {
    server.close();
  });

  describe('findDistrict', () => {
    it('should return 404 and message NOT_FOUND if address is not found in districts', () => {
      const address = "Pub Fiction";

      when(mockAddressToCoordinateService.getCoordinatesForAddress).mockReturnValue(Promise.resolve(undefined));
      
      when(mockDistrictService.findDistrict).
        mockReturnValue(undefined);

      request(server.getHttpServer())
        .get(`/geo-finder/district/${address}`)
        .expect(404, {
            "status": "NOT_FOUND",
            "search": "Non-existing address"
        });
    });

    it('should return 200 and object if address and district is found', () => {
        const address = "Pub Fiction";

        const address1 = "address1 somewshere";
        const city = "Białystok";
        const lat = 11;
        const lng = 0.2;
        const postcode = "123-23"
        const serviceAddress = "Cool district";

        when(mockAddressToCoordinateService.getCoordinatesForAddress).
          mockReturnValue(Promise.resolve({
            address1,
            city,
            lat,
            lng,
            postcode
          }))
  
        when(mockDistrictService.findDistrict).
          mockReturnValue(serviceAddress);
  
        return request(server.getHttpServer())
          .get(`/geo-finder/district/${address}`)
          .expect(200, {
              status: "OK",
              search: address,
              location: {
                  address1, 
                  city, 
                  lat, 
                  lng,
                  postcode, 
                  serviceAddress
              }
          });
      });
  });
});
