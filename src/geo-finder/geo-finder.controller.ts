import { CacheInterceptor, Controller, Get, HttpStatus, Param, Res, UseInterceptors, ValidationPipe } from "@nestjs/common";
import { DistrictService } from "../district/district.service";
import { AddressToCoordinateService } from "../address-to-coordinate/address-to-coordinate.service";
import { Response } from "express";

@Controller('geo-finder')
@UseInterceptors(CacheInterceptor)
export class GeoFinderController {
    constructor( 
        private readonly addressToCoordinate: AddressToCoordinateService,
        private readonly districtService: DistrictService,
    ) {}

    @Get('district/:address')
    async findDistrict(
        @Param('address', new ValidationPipe({
            transform: true,
            forbidNonWhitelisted: true,
            forbidUnknownValues: true
        })) address: string,
        @Res() response: Response
    ) {
        const data = await this.addressToCoordinate.getCoordinatesForAddress(address);

        const district = this.districtService.findDistrict(data.lat, data.lng)

     
        if (!district) {
            response.status(HttpStatus.NOT_FOUND).send({
                status: "NOT_FOUND",
                search: "Non-existing address"
            });
        }

        response.status(200).send({
            status: "OK", 
            search: address, 
            location: {
                ...data, 
                serviceAddress: district
            }
        });
    }
}