import { CacheModule, Module } from "@nestjs/common";
import { DistrictModule } from "../district/district.module";
import { AddressToCoordinateModule } from "../address-to-coordinate/address-to-coordinate.module";
import { GeoFinderController } from "./geo-finder.controller";

@Module({
    imports: [
        CacheModule.register({ ttl: 60, max: 1000 }), 
        AddressToCoordinateModule, 
        DistrictModule
    ],
    controllers: [GeoFinderController], 
    providers: []
})
export class GeoFinderModule {}