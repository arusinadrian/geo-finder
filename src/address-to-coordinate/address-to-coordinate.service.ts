import { HttpService } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { FindPlaceApiResponse } from "./interfaces/findPlaceApiResponse";
import { lastValueFrom } from 'rxjs';
import { Client, Language } from "@googlemaps/google-maps-services-js";

@Injectable()
export class AddressToCoordinateService {
    client: Client;
    constructor(private httpService: HttpService, private configService: ConfigService) {
        this.client = new Client();
    }

    async getCoordinatesForAddress(address: string) {
        const encodedAddress = encodeURIComponent(address);

        const googlePlaceUrl = this.configService.get('googlePlaceApiUrl', { infer: true });
        const googlePlaceKey = <string>this.configService.get('googlePlaceApiKey', { infer: true });
        const additionalParams = '&input&inputtype=textquery&language=en&fields=place_id';

        const url = `${googlePlaceUrl}/json?input=${encodedAddress}${additionalParams}&key=${googlePlaceKey}`;

        const response =  await lastValueFrom(this.httpService.get<FindPlaceApiResponse>(url, { headers: {}}));

        const result = response.data.candidates[0];

        const placeDetail = await this.client.placeDetails({
            params: {
                place_id: result.place_id,
                language: Language.en,
                key: googlePlaceKey,
            },
        });
        
        if (!placeDetail?.data?.result) {
            throw new Error('Could not find the place');
        }

        return {
            address1: placeDetail.data.result.address_components[0].long_name,
            city: placeDetail.data.result.address_components[1].long_name,
            lat: placeDetail.data.result.geometry.location.lat,
            lng: placeDetail.data.result.geometry.location.lng,
            postcode: placeDetail.data.result.address_components[5].long_name
        }
    }
}