import { HttpModule, HttpService } from "@nestjs/axios";
import { ConfigService } from "@nestjs/config";
import { Test, TestingModule } from "@nestjs/testing";
import { AddressToCoordinateService } from "./address-to-coordinate.service";
import { when } from "jest-when";
import { of } from "rxjs";

const mockedPlaceDetail = jest.fn();

jest.mock("@googlemaps/google-maps-services-js", () => ({
    Client: class {
        placeDetails() {
            return jest.fn().mockReturnValue(Promise.resolve({
                'test': 123,
                data: {
                    result: 123132
                }
            }));
        }
    }, 
    Language: {
        "en": "EN"
    }
}));

describe('AddressToCoordinateService tests', () => {
    let service: AddressToCoordinateService;

    let mockHttpService = { get: jest.fn() }

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [HttpModule], 
            providers: [AddressToCoordinateService, ConfigService],
        })
        .overrideProvider(HttpService)
        .useValue(mockHttpService)
        .compile();

        service = module.get<AddressToCoordinateService>(AddressToCoordinateService);
    })

    it('should return error if it couldnt find a place', async () => {
        when(mockHttpService.get).mockReturnValue(of({
            data: {
                "candidates": [
                    {
                        "formatted_address": "White Bear Yard, London EC1R 5DP, UK",
                        "geometry": {
                            "location": {
                                "lat": 51.5222691,
                                "lng": -0.1098115
                            },
                            "viewport": {
                                "northeast": {
                                    "lat": 51.52361892989272,
                                    "lng": -0.1084616701072778
                                },
                                "southwest": {
                                    "lat": 51.52091927010727,
                                    "lng": -0.1111613298927222
                                }
                            }
                        }
                    }
                ],
                "status": "OK"
            }
        }));

        return expect(service.getCoordinatesForAddress('Pub Fiction')).rejects.toEqual(new Error('Could not find the place'));
    });

    it.skip('should return information about given address with its city, latitude and longitude, plus_code(postal_code), as well as street', async () => {
        mockedPlaceDetail.mockReturnValue(Promise.resolve({
            'test': 123,
            data: {
                result: 123132
            }
        }));

        
        when(mockHttpService.get).mockReturnValue(of({
            data: {
                "candidates": [
                    {
                        "formatted_address": "White Bear Yard, London EC1R 5DP, UK",
                        "geometry": {
                            "location": {
                                "lat": 51.5222691,
                                "lng": -0.1098115
                            },
                            "viewport": {
                                "northeast": {
                                    "lat": 51.52361892989272,
                                    "lng": -0.1084616701072778
                                },
                                "southwest": {
                                    "lat": 51.52091927010727,
                                    "lng": -0.1111613298927222
                                }
                            }
                        }
                    }
                ],
                "status": "OK"
            }
        }));

        const result = await service.getCoordinatesForAddress('Pub Fiction');

       
        expect(result.address1).toEqual("Suraska 1, Pub Fiction"); 
        expect(result.city).toEqual('London');
        expect(result.lat).toEqual(51.5222691);
        expect(result.lng).toEqual(-0.1098115);
        expect(result.postcode).toEqual('EC1R5DP');
    });
});