import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { AddressToCoordinateService } from "./address-to-coordinate.service";

@Module({
    imports: [HttpModule],
    providers: [AddressToCoordinateService],
    exports: [AddressToCoordinateService],
})
export class AddressToCoordinateModule {}