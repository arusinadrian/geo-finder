export interface FindPlaceApiResponse {
    candidates?: (CandidatesEntity)[] | null;
}

export interface CandidatesEntity {
    place_id: string;
}