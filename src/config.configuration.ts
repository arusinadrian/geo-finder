export default () => (<Config>{
    googlePlaceApiKey: process.env.GOOGLE_MAPS_API_KEY|| '',
    googlePlaceApiUrl: process.env.GOOGLE_PLACE_URL || 'https://maps.googleapis.com/maps/api/place/findplacefromtext'
});

export interface Config {
    googlePlaceApiKey: string,
    googlePlaceApiUrl: string,
}