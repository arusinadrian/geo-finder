import { Injectable } from "@nestjs/common";
import formattedDistricts from "./formatted-districts.json";

@Injectable()
export class DistrictService {
    coordinateToDistrict: Map<string, string> = new Map();

    onModuleInit() {
        formattedDistricts.features.forEach((feature) => {
            feature.geometry.coordinates[0].forEach((coordinate) => {
                this.coordinateToDistrict.set(`${coordinate[0]}, ${coordinate[1]}`, feature.properties.Name);
            })
        })
    }


    findDistrict(lat: number, lng: number) {
        return this.coordinateToDistrict.get(`${lat}, ${lng}`);
    }
}