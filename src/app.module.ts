import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DistrictModule } from './district/district.module';
import configuration from './config.configuration';
import { GeoFinderModule } from './geo-finder/geo-finder.module';

@Module({
  imports: [
    DistrictModule, 
    GeoFinderModule, 
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
