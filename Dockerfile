FROM node:16-bullseye

USER root:root
RUN mkdir /geo-finder && chown -R node:node /geo-finder 

USER node:node
WORKDIR /geo-finder 

COPY --chown=node:node . . 
RUN yarn install

RUN yarn run build

CMD ["yarn", "start"]